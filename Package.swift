// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ThreeInRowCore",
    products:
        [
            .library(
                name: "ThreeInRowCoreFrontend",
                targets:
                    [
                        "ThreeInRowCoreFrontend"
                    ]
            ),
            .library(
                name: "ThreeInRowCoreBackend",
                targets:
                    [
                        "ThreeInRowCoreBackend"
                    ]
            ),
            .library(
                name: "ThreeInRowCoreGeneric",
                targets:
                    [
                        "ThreeInRowCoreGeneric"
                    ]
            ),
            .library(
                name: "ThreeInRowCore",
                targets:
                    [
                        "ThreeInRowCore"
                    ]
            ),
        ],
    dependencies:
        [
            .package(
                name: "MathKit",
                url: "https://gitlab.com/ArMuSebastian/MathKit",
                .branch("master")
            ),
            .package(
                name: "ChainDetector",
                url: "https://gitlab.com/ArMuSebastian/ChainDetector",
                .branch("master")
            ),
        ],
    targets:
        [
            .target(
                name: "ThreeInRowCoreGeneric",
                dependencies:
                    [
                        "ChainDetector",
                    ]
            ),
            .target(
                name: "ThreeInRowCore",
                dependencies:
                    [
                        "ThreeInRowCoreGeneric",
                        "ChainDetector",
                        "MathKit",
                    ]
            ),
            .target(
                name: "ThreeInRowCoreFrontend",
                dependencies:
                    [
                        "MathKit",
                        "ThreeInRowCore",
                    ]
            ),
            .target(
                name: "ThreeInRowCoreBackend",
                dependencies:
                    [
                        "MathKit",
                        "ThreeInRowCore",
                        "ChainDetector",
                    ]
            ),
            .testTarget(
                name: "ThreeInRowCoreTests",
                dependencies:
                    [
                        "ThreeInRowCore"
                    ]
            ),
        ]
)
