//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

import Foundation

import MathKit
import ThreeInRowCoreGeneric

public enum MathKitMoodule {

    // MARK: - Matrix
    public typealias Matrix<T> = MathKit.Matrix<T>
    public typealias Index = Matrix<Any>.Index
    public typealias Size = MathKit.Size

    // MARK: - Coordinate
    public typealias Axis = MathKit.Axis
    public typealias Direction = Axis.Direction
    public typealias Step = Direction.Step

}


extension Index: ChainDetectorModule.Key {

    public typealias AxisPredicate = Int
    
    public typealias Axis = MathKitMoodule.Axis

    public func predicate(for axis: Axis) -> Int {
        switch axis {
        case .vertical:
            return self.column
        case .horisontal:
            return self.row
        }
    }
    
    public func applying(delta: Axis.Direction.Step) -> Self {
        self + delta
    }

    static
    public func + (index: Self, step: Axis.Direction.Step) -> Self {
        return Self(row: index.row + step.vertical,
                    column: index.column + step.horisontal)
    }

    static
    public func + (step: Axis.Direction.Step, index: Self) -> Self {
        return index + step
    }

}
