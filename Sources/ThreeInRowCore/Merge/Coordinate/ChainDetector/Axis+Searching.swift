//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

import ThreeInRowCoreGeneric

extension MathKitMoodule.Axis.Direction: ChainDetectorModule.Direction {

}

extension MathKitMoodule.Axis: ChainDetectorModule.Axis {
    
}

extension MathKitMoodule.Axis.Direction.Step: ChainDetectorModule.Step {

    // Note: Copy in mathkit
    static
    public func * (
        directionDelta: Self,
        multiplier: Int
    ) -> Self {
        return Self(horisontal: directionDelta.horisontal * multiplier,
                     vertical: directionDelta.vertical * multiplier)
    }

    static
    public func * (
        multiplier: Int,
        directionDelta: Self
    ) -> Self {
        return Self(horisontal: directionDelta.horisontal * multiplier,
                     vertical: directionDelta.vertical * multiplier)
    }

}
