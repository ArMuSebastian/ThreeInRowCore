//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

import ThreeInRowCoreGeneric

public typealias ElementType = ElementKindInformation
public typealias Element = ElementInformation<ElementType>

extension ElementType: ChainDetectorModule.ElementType { }
extension Element: ChainDetectorModule.Element { }
