//
//  Swap.swift
//  
//
//  Created by Artem Myshkin on 18.07.2021.
//

import Foundation

public struct Swap: Action {

}

//
//    let firstElement: Element
//    let secondElement: Element
//
//}
//
//extension Swap: CustomStringConvertible {
//
//    var description: String {
//        return "Swap \(firstElement) with \(secondElement)"
//    }
//
//}
//
//extension Swap: Hashable {
//
//    func hash(into hasher: inout Hasher) {
//
//        if firstElement > secondElement {
//            hasher.combine(firstElement)
//            hasher.combine(secondElement)
//        } else {
//            hasher.combine(secondElement)
//            hasher.combine(firstElement)
//        }
//
//    }
//
//}
//
//extension Swap: Equatable {
//
//    static func == (lhs: Swap, rhs: Swap) -> Bool {
//        return
//            (lhs.firstElement === rhs.firstElement, lhs.secondElement === rhs.secondElement)
//            ||
//            (lhs.firstElement === rhs.secondElement, lhs.secondElement === rhs.firstElement)
//    }
