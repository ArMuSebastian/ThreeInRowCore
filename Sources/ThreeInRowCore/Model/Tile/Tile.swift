//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

import ThreeInRowCoreGeneric

public typealias TileType = TileKindInformation
public typealias Tile = TileInformation<TileType>

extension TileType: ChainDetectorModule.TileType { }
extension Tile: ChainDetectorModule.Tile { }
