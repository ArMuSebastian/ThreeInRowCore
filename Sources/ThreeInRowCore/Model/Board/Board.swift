//
//  Board.swift
//  
//
//  Created by Artem Myshkin on 22.07.2021.
//

import ThreeInRowCoreGeneric
import ChainDetector
import MathKit

public typealias Board = ThreeInRowCoreGeneric.Board<BoardStruct> 

extension Board: ChainDetectorModule.Searchable {

}
