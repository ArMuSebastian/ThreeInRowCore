//
//  File.swift
//  
//
//  Created by Artem Myshkin on 06.03.2022.
//

import ThreeInRowCoreGeneric
import MathKit

public typealias BoardStruct = Matrix<BoardCell>

extension BoardStruct: OneLevelStructure {

    public func contains(_ key: Self.Index) -> Bool {
        self.indices.contains(key)
    }

}
