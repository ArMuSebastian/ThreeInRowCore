//
//  File.swift
//  
//
//  Created by Artem Myshkin on 06.03.2022.
//

import ThreeInRowCoreGeneric
import MathKit

public typealias BoardCell = CellContainer<Element, Tile>

extension BoardCell: ChainDetectorModule.CellContainer {

}
