//
//  Chain.swift
//  
//
//  Created by Artem Myshkin on 19.07.2021.
//
import ThreeInRowCoreGeneric
import MathKit
import ChainDetector

extension Index.Axis: Kek {}
public typealias Chain = ThreeInRowCoreGeneric.Chain<Element, Index, Index.Axis>

extension Chain: ChainDetectorModule.Combination {}
