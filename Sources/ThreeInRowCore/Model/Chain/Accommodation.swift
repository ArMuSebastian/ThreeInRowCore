//
//  Accommodation.swift
//  
//
//  Created by Artem Myshkin on 21.07.2021.
//

import ThreeInRowCoreGeneric
import MathKit


public typealias Accommodation = ThreeInRowCoreGeneric.Accommodation<Element, Index>

extension Accommodation: ChainDetectorModule.Echalon { }
