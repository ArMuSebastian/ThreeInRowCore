//
//  SingleValueDecodingContainer+Decode.swift
//  
//
//  Created by Artem Myshkin on 05.09.2021.
//

import Foundation

extension SingleValueDecodingContainer {

    func decode<T: Decodable>() throws -> T {
        return try self.decode(T.self)
    }

}
