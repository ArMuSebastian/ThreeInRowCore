//
//  Matrix+Codable.swift
//  
//
//  Created by Artem Myshkin on 05.09.2021.
//

import Foundation
import MathKit

extension Matrix {

    struct MatrixPayload<Element> {

        let elements: [Element]
        let stride: Int

    }

}

extension Matrix.MatrixPayload: Decodable where Element: Decodable { }
extension Matrix.MatrixPayload: Encodable where Element: Encodable { }

extension Matrix: Decodable where Element: Decodable {

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let matrixPayload: MatrixPayload<Element> = try container.decode()

        let payload = matrixPayload.elements.chunked(by: matrixPayload.stride)
        self.init(array: payload)!

    }

}

extension Matrix: Encodable where Element: Encodable {

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        let elements = self[self.indices]
        let size = self.size
        let matrixPayload: MatrixPayload<Element> = .init(elements: elements, stride: size.columns)
        try container.encode(matrixPayload)

    }

}

extension Array {

    internal func chunked(by size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

}
