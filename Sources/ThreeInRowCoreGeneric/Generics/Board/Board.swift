public struct Board<GenericStructure: OneLevelStructure> {

    public typealias Structure = GenericStructure

    private var elements: Structure

    public var indices: [Structure.Key] {
        return elements.indices
    }

    public init(with structure: Structure) {
        self.elements = structure
    }

    public subscript(index: Structure.Key) -> Structure.Element {
        get {
            return elements[index]
        }
        set {
            elements[index] = newValue
        }
    }

    public func contains(_ key: Structure.Key) -> Bool {
        elements.contains(key)
    }

}

extension Board: CustomStringConvertible {

    public var description: String {
        return "\(elements)"
    }

}
