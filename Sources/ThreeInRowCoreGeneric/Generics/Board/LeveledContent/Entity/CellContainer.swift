public struct CellContainer<Element, Tile>: CellContent {

    public typealias FirstLevel = Element
    public typealias SecondLevel = Tile
    
    public var first: FirstLevel?
    public var second: SecondLevel

    public init(
        first: FirstLevel?,
        second: SecondLevel
    ) {
        self.first = first
        self.second = second
    }

}
