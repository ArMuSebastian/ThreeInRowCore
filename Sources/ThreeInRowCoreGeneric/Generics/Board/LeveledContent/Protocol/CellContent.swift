//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

public protocol CellContent {

    associatedtype FirstLevel
    associatedtype SecondLevel

    var first: FirstLevel? { get }
    var second: SecondLevel { get }

    init(first: FirstLevel?, second: SecondLevel)

}
