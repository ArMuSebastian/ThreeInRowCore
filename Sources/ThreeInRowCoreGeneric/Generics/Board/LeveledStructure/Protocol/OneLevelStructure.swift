//
//  OneLevelStructure.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

public protocol OneLevelStructure {

    associatedtype Key
    associatedtype Element

    var indices: [Key] { get }

    func contains(_ key: Key) -> Bool
    subscript(_ key: Key) -> Element { get set }

}
