
public struct Accommodation<Element: Hashable, Key: Hashable> {

    public let element: Element
    public let key: Key

    public init(element: Element, key: Key) {
        self.element = element
        self.key = key
    }

}

extension Accommodation: Equatable {

    static
    public func == (lhs: Self, rhs: Self) -> Bool {
        return
            lhs.element == rhs.element
            &&
            lhs.key == rhs.key
    }

}

extension Accommodation: Hashable {

}
