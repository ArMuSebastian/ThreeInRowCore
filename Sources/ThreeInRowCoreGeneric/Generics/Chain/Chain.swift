
public protocol Kek: Hashable {

    static var vertical: Self { get }
    static var horisontal: Self { get }

}

public struct Chain<Element: Hashable, Key: Hashable&Comparable, Axis: Kek> {

    public typealias Accomodation = Accommodation<Element, Key>

    private(set) public var type: Axis
    private(set) public var elements: [Accomodation]

    public init(elements: [Accomodation], type: Axis) {
        self.elements = elements
        self.type = type
    }

}

extension Chain: Comparable {

    // chain 1 bigger than chain 2
    // if chain 1 most top element if topper that chain 2 most top element
    // if chain 1 most left element if lefter that chain 2 most left element
    static
    public func < (lhs: Self, rhs: Self) -> Bool {
        switch (lhs.type, rhs.type) {
        case (.vertical, .horisontal):
            return lhs.elements.last!.key < rhs.elements.first!.key
        case (.horisontal, .vertical):
            return lhs.elements.first!.key < rhs.elements.last!.key
        case (.horisontal, .horisontal), (.vertical, .vertical):
            return lhs.elements.first!.key < rhs.elements.first!.key
        default:
            fatalError()
        }
    }

}

extension Chain: Equatable {

    static
    public func == (lhs: Self, rhs: Self) -> Bool {
        return
            lhs.elements == rhs.elements
            &&
            lhs.type == rhs.type
    }

}

extension Chain: Hashable {

}
