//
//  EntityRequirement.swift
//  
//
//  Created by Artem Myshkin on 15.08.2021.
//

public protocol EntityRequirement: Hashable {

    associatedtype Kind: EntityTypeRequirement

    var type: Kind { get }

}
