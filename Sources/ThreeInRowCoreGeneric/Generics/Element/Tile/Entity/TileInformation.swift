//
//  TileInformation.swift
//
//
//  Created by Artem Myshkin on 05.09.2021.
//

public struct TileInformation<TileKind: TileTypeRequirement>: TileRequirement {

//    enum CodingKeys: String, CodingKey {
//        case type
//    }

    public var type: Kind

    public typealias Kind = TileKind

    public init(from type: Kind) {
        self.type = type
    }

}

extension TileInformation: CustomStringConvertible {

    public var description: String {
        "\(type)"
    }

}

//extension TileInformation: Codable {
//
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        let type = try container.decode(Kind.self, forKey: .type)
//        self.init(from: type)
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(type, forKey: .type)
//    }
//
//}
