//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

import Foundation

public struct TileKindInformation: TileTypeRequirement {

//    enum CodingKeys: String, CodingKey {
//        case rawValue
//    }

    public static var hole: Self = .init(from: 0)

    public var rawValue: Int

    public init(from rawValue: Int) {
        self.rawValue = rawValue
    }

}

//extension TileKindInformation: Decodable {
//
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        let value = try container.decode(Int.self, forKey: .rawValue)
//        self.init(from: value)
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(rawValue, forKey: .rawValue)
//    }
//
//}
