//
//  TileRequirement.swift
//  
//
//  Created by Artem Myshkin on 15.08.2021.
//

public protocol TileRequirement: Hashable, Codable {

    associatedtype Kind: TileTypeRequirement

    var type: Kind { get }

}
