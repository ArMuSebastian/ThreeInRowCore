//
//  TileTypeRequirement.swift
//  
//
//  Created by Artem Myshkin on 15.08.2021.
//

public protocol TileTypeRequirement: Hashable, Codable {

    static var hole: Self { get }

}
