//
//  File.swift
//  
//
//  Created by Artem Myshkin on 05.03.2022.
//

import Foundation
import ChainDetector

public enum ChainDetectorModule {

    public typealias Detector = ChainDetector.Module
    public typealias Core = Detector.Core

    // MARK: - Board
    public typealias Searchable = Core.CDSearchable
    public typealias CellContainer = Core.CDBoardCell

    // MARK: - Combo
    public typealias Combination = Core.CDCombination
    public typealias Echalon = Core.CDEchalon

    // MARK: - ELEMENT
    public typealias Element = Core.CDElement
    public typealias ElementType = Core.CDElementType

    // MARK: - Structure
    public typealias Axis = Core.CDAxis
    public typealias Direction = Core.CDDirection
    public typealias Step = Core.CDStep

    public typealias Key = Core.CDKey

    // MARK: - Tile
    public typealias Tile = Core.CDTile
    public typealias TileType = Core.CDTileType

}
