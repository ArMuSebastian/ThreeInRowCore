extension TestableThings {

    struct Board {

        typealias TestBoard = ThreeInRowCore.Board

        private init() {}

        static func from(
            payload: Self.Payload
        ) -> TestBoard {
            return create(from: payload)
        }

    }

}

extension TestableThings.Board {

    static func create(
        from structure: TestBoard.Structure
    ) -> ThreeInRowCore.Board {
        
        TestBoard(with: structure)
    }

    static func create(
        from payload: Self.Payload
    ) -> TestBoard
    {
        var indices: [TestBoard.Key]?
        
        indices = (payload.mask.indices == payload.elements.indices) ? payload.mask.indices : nil
        guard let safeIndices = indices else {
            fatalError("HERrE")
        }
        
        let content = zip(payload.elements[safeIndices], payload.mask[safeIndices])
            .map {
                TestBoard.CellContent.init(first: $0, second: $1)
            }

        
        let m: TestBoard.Structure = TestBoard.Structure(array: content.chunked(by: payload.elements.size.columns))!
        
        return self.create(from: m)
    }

}


extension Array {

    internal func chunked(by size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

}
