extension TestableThings.Board {

    struct Payload {

        typealias ElementsMatrix = Matrix<Element>
        typealias TileMatrix = Matrix<Tile>

        let elements: ElementsMatrix
        let mask: TileMatrix

        private init(
            elements: ElementsMatrix,
            mask: TileMatrix
        ) {
            self.elements = elements
            self.mask = mask
        }

        static var h1: Self {

            let types = createTypes(
                [
                    [.init(from: 0), .init(from: 1), .init(from: 2), .init(from: 3)],
                    [.init(from: 1), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [.init(from: 2), .init(from: 3), .init(from: 0), .init(from: 1)],
                    [.init(from: 0), .init(from: 0), .init(from: 0), .init(from: 0)]
                ]
            )
            print(types)

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var h2: Self {

            let types = createTypes(
                [
                    [.init(from: 0), .init(from: 1), .init(from: 2), .init(from: 3)],
                    [.init(from: 1), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [.init(from: 0), .init(from: 0), .init(from: 0), .init(from: 0)],
                    [.init(from: 2), .init(from: 3), .init(from: 0), .init(from: 1)],
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var h3: Self {

            let types = createTypes(
                [
                    [.init(from: 0), .init(from: 0), .init(from: 0), .init(from: 0)],
                    [.init(from: 0), .init(from: 1), .init(from: 2), .init(from: 3)],
                    [.init(from: 1), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [.init(from: 2), .init(from: 3), .init(from: 0), .init(from: 1)],
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }


        static var v1: Self {

            let types = createTypes(
                [
                    [.init(from: 0), .init(from: 1), .init(from: 2), .init(from: 3)],
                    [.init(from: 1), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [.init(from: 2), .init(from: 3), .init(from: 0), .init(from: 3)],
                    [.init(from: 0), .init(from: 1), .init(from: 2), .init(from: 3)]
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var v2: Self {

            let types = createTypes(
                [
                    [.init(from: 0), .init(from: 1), .init(from: 3), .init(from: 2)],
                    [.init(from: 1), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [.init(from: 2), .init(from: 3), .init(from: 3), .init(from: 0)],
                    [.init(from: 0), .init(from: 1), .init(from: 3), .init(from: 2)]
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var v3: Self {

            let types = createTypes(
                [
                    [ .init(from: 3), .init(from: 0), .init(from: 1), .init(from: 2)],
                    [ .init(from: 3), .init(from: 1), .init(from: 2), .init(from: 3)],
                    [ .init(from: 3), .init(from: 2), .init(from: 3), .init(from: 0)],
                    [ .init(from: 3), .init(from: 0), .init(from: 1), .init(from: 2)]
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var d1: Self {

            let types = createTypes(
                [
                    [ .init(from: 3), .init(from: 0), .init(from: 1), .init(from: 2)],
                    [ .init(from: 3), .init(from: 3), .init(from: 3), .init(from: 3)],
                    [ .init(from: 3), .init(from: 2), .init(from: 3), .init(from: 0)],
                    [ .init(from: 3), .init(from: 0), .init(from: 1), .init(from: 2)]
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var d2: Self {

            let types = createTypes(
                [
                    [ .init(from: 3), .init(from: 0), .init(from: 3), .init(from: 3)],
                    [ .init(from: 3), .init(from: 3), .init(from: 3), .init(from: 3)],
                    [ .init(from: 3), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [ .init(from: 3), .init(from: 0), .init(from: 3), .init(from: 2)]
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.fromSize(elements.size))
            return Self.init(elements: elements, mask: mask)
        }

        static var hole1: Self {

            let types = createTypes(
                [
                    [.init(from: 0), .init(from: 1), .init(from: 2), .init(from: 3)],
                    [.init(from: 0), .init(from: 0), .init(from: 0), .init(from: 0)],
                    [.init(from: 1), .init(from: 2), .init(from: 3), .init(from: 3)],
                    [.init(from: 2), .init(from: 3), .init(from: 0), .init(from: 1)],
                ]
            )

            let elements = createElementsMatrix(from: types)
            let mask = createMaskMatrix(.some(
                [
                    [.init(from: 1), .init(from: 1), .init(from: 1), .init(from: 1)],
                    [.init(from: 1), .init(from: 0), .init(from: 1), .init(from: 1)],
                    [.init(from: 1), .init(from: 1), .init(from: 1), .init(from: 1)],
                    [.init(from: 1), .init(from: 1), .init(from: 1), .init(from: 1)],
                ]
            ))
            return Self.init(elements: elements, mask: mask)
        }

    }

}

extension TestableThings.Board.Payload {

    private enum MaskCreation {
        case some([[TileMatrix.Element.Kind]])
        case fromSize(MathKit.Size)
    }

    static
    private func createTypes(_ types: [[ElementsMatrix.Element.Kind]]) -> [[ElementsMatrix.Element.Kind]] {

        return types
    }

    static
    private func createElementsMatrix(from types: [[ElementsMatrix.Element.Kind]]) -> ElementsMatrix {
        return matrix(content: types.map { $0.map(ElementsMatrix.Element.init(from:)) })
    }

    static
    private func createMaskMatrix(_ creation: MaskCreation) -> TileMatrix {
        switch creation {
        case .fromSize(let size):
            let tiles = (0..<size.columns).map { _ in
                return (0..<size.rows).map { _ in return TileMatrix.Element(from: TileMatrix.Element.Kind.init(from: 1)) }
            }
            return matrix(content: tiles)
        case .some(let types):
            return matrix(content: types.map { $0.map { TileMatrix.Element.init(from: $0) } } )
        }
    }

    static
    private func matrix<T>(content: [[T]]) -> Matrix<T> {
        return Matrix(array: content)!
    }

}
