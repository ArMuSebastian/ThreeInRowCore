extension TestableThings {

    struct ChainDetector {

        struct TestCase {

            typealias Board = TestableThings.Board.TestBoard
            typealias Chaining = ThreeInRowCore.Chain
            typealias Index = Board.Structure.Key

            var board: Board
            var indices: [Index]
            var result: [Chaining]

        }

        private init() {}

        static var allCases: [TestCase] {
            return
                [
                    c1, c2, c3,
                    c4, c5, c6,
                ]
        }
        
        static var c1: TestCase {

            return composeTest(indices: [(0...3).map { .init(row: 3, column: $0) }],
                               board: TestableThings.Board.create(from: .h1))
        }

        static var c2: TestCase {

            return composeTest(indices: [(0...3).map { .init(row: 2, column: $0) }],
                               board: TestableThings.Board.create(from: .h2))
        }

        static var c3: TestCase {

            return composeTest(indices: [(0...3).map { .init(row: 0, column: $0) }],
                               board: TestableThings.Board.create(from: .h3))
        }

        
        static var c4: TestCase {

            return composeTest(indices: [(0...3).map { .init(row: $0, column: 3) }],
                               board: TestableThings.Board.create(from: .v1))
        }

        static var c5: TestCase {

            return composeTest(indices: [(0...3).map { .init(row: $0, column: 2) }],
                               board: TestableThings.Board.create(from: .v2))
        }

        static var c6: TestCase {

            return composeTest(indices: [(0...3).map { .init(row: $0, column: 0) }],
                               board: TestableThings.Board.create(from: .v3))
        }

        static var c7: TestCase {

            return composeTest(indices:
                                [(0...3).map { .init(row: $0, column: 0) }]
                                +
                                [(0...3).map { .init(row: 1, column: $0) }],
                               board: TestableThings.Board.create(from: .d1))
        }

        static var c8: TestCase {

            return composeTest(indices:
                                [(0...3).map { .init(row: $0, column: 0) }]
                                +
                                [(0...3).map { .init(row: 1, column: $0) }]
                                +
                                [(0...3).map { .init(row: $0, column: 2) }]
                                +
                                [(0...2).map { .init(row: $0, column: 3) }],
                               board: TestableThings.Board.create(from: .d2))
        }

        static var c9: TestCase {

            return composeTest(indices: [(1...3).map { .init(row: 1, column: $0) }],
                               board: TestableThings.Board.create(from: .hole1),
                               producesChains: false)
        }

    }

}

extension TestableThings.ChainDetector {

    static private func chainsFrom(
        board: TestCase.Board,
        indices: [[TestCase.Index]]
    ) -> [TestCase.Chaining] {
        let chains: [TestCase.Chaining] = indices.map { chainIndices -> TestCase.Chaining in
            let elements = chainIndices.map { index -> ThreeInRowCore.Accommodation in
                let e = board[index].first!
                return Accommodation(element: e, key: index)
            }
            
            if Set(chainIndices.map({ $0.predicate(for: .vertical) })).count == 1 {
                return Chain(elements: elements, type: .vertical)
            } else if Set(chainIndices.map({ $0.predicate(for: .horisontal) })).count == 1 {
                return Chain(elements: elements, type: .horisontal)
            }
            
            fatalError()
        }
        return chains
    }


    static private func composeTest(
        indices: [[TestCase.Index]],
        board: TestCase.Board,
        producesChains: Bool = true
    ) -> TestCase {
        let result = producesChains ? chainsFrom(board: board, indices: indices) : []

        return TestCase(
            board: board,
            indices: indices.reduce([]) { $0 + $1 },
            result: result.sorted()
        )
    }

}
